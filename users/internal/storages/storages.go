package storages

import (
	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/db/adapter"
	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/infrastructure/cache"
	ustorage "gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/modules/storage"
)

type Storages struct {
	User ustorage.Userer
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User: ustorage.NewUserStorage(sqlAdapter, cache),
	}
}

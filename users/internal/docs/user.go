// nolint:all
package docs

import (
	ucontroller "gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/modules/controller"
)

// swagger:route GET /api/1/user/profile user profileRequest
// Получение информации о текущем пользователе
// security:
//	- Bearer: []
// responses:
//	200: profileResponse

// swagger:response profileResponse
type profileResponse struct {
	// in:body
	Body ucontroller.ProfileResponse
}

// swagger:route POST /api/1/user/reset_password user changePasswordRequest
// Смена пароля текущего пользователя
// security:
//	- Bearer: []
// responses:
//	200: changePasswordResponse

// swagger:parameters
type changePasswordRequest struct {
	// in:body
	Body ucontroller.ChangePasswordRequest
}

// swagger:response profileResponse
type changePasswordResponse struct {
	// in:body
	Body ucontroller.ChangePasswordResponse
}

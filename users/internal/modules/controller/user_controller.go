package controller

import (
	"net/http"

	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/models"

	"github.com/ptflp/godecoder"
	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/infrastructure/component"
	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/infrastructure/errors"
	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/infrastructure/handlers"
	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/infrastructure/responder"
	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/modules/service"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
	ChangePassword(w http.ResponseWriter, r *http.Request)
	GetByEmail(w http.ResponseWriter, r *http.Request)
	CreateUser(w http.ResponseWriter, r *http.Request)
	GetByID(w http.ResponseWriter, r *http.Request)
	VerifyEmail(w http.ResponseWriter, r *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var req service.ChangePasswordIn

	err := u.Decode(r.Body, &req)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	if len(req.NewPassword) == 0 {
		u.OutputJSON(w, ChangePasswordResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: Data{
				Message: "new password field is empty",
			},
		})
	}

	result := u.service.ChangePassword(r.Context(), req)

	if result.ErrorCode == errors.AuthServiceWrongPasswordErr {
		u.OutputJSON(w, ChangePasswordResponse{
			Success:   false,
			ErrorCode: result.ErrorCode,
			Data: Data{
				Message: "wrong old password",
			},
		})
		return
	}
	if result.ErrorCode != errors.NoError {
		u.OutputJSON(w, ChangePasswordResponse{
			Success:   false,
			ErrorCode: result.ErrorCode,
			Data: Data{
				Message: "wrong old or new password",
			},
		})
		return
	}

	u.OutputJSON(w, ChangePasswordResponse{
		Success:   true,
		ErrorCode: result.ErrorCode,
		Data: Data{
			Message: "password changed successfully",
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (u *User) GetByEmail(w http.ResponseWriter, r *http.Request) {
	var (
		err error
		in  service.GetByEmailIn
		out service.UserOut
	)

	err = u.Decode(r.Body, &in)
	if err != nil {
		u.ErrorBadRequest(w, err)
	}

	out = u.service.GetByEmail(r.Context(), in)
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "get by email error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Message: "get by email success",
			User:    *out.User,
		},
	})
}

func (u *User) CreateUser(w http.ResponseWriter, r *http.Request) {
	var (
		err    error
		in     service.UserCreateIn
		result service.UserCreateOut
	)

	err = u.Decode(r.Body, &in)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	result = u.service.Create(r.Context(), in)
	if result.ErrorCode != errors.NoError {
		u.OutputJSON(w, CreateResponse{
			Success:   false,
			ErrorCode: result.ErrorCode,
			Data: Data{
				Message: "create user error",
			},
		})
	}

	u.OutputJSON(w, CreateResponse{
		Success:   true,
		ErrorCode: result.ErrorCode,
		Data: Data{
			Message: "create user OK",
			User: models.User{
				ID: result.UserID,
			},
		},
	})
}

func (u *User) GetByID(w http.ResponseWriter, r *http.Request) {
	var (
		err error
		in  service.GetByIDIn
		out service.UserOut
	)

	err = u.Decode(r.Body, &in)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	out = u.service.GetByID(r.Context(), in)
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "get by id error",
			},
		})
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Message: "get by id success",
			User:    *out.User,
		},
	})
}

func (u *User) VerifyEmail(w http.ResponseWriter, r *http.Request) {
	var (
		err error
		in  service.UserVerifyEmailIn
		out service.UserUpdateOut
	)

	err = u.Decode(r.Body, &in)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	out = u.service.VerifyEmail(r.Context(), in)
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, VerifyResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "email verify error",
			},
		})
	}

	u.OutputJSON(w, VerifyResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Message: "email verify success",
		},
	})
}

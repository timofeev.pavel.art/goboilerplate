package modules

import (
	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/infrastructure/component"
	ucontroller "gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/modules/controller"
)

type Controllers struct {
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		User: userController,
	}
}

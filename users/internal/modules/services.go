package modules

import (
	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/infrastructure/component"
	uservice "gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/modules/service"
	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/storages"
)

type Services struct {
	User uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
	}
}

package router

import (
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/infrastructure/component"
	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/infrastructure/middleware"
	"gitlab.com/timofeev.pavel.art/goboilerplate/users/internal/modules"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
			authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)
			r.Route("/user", func(r chi.Router) {
				userController := controllers.User
				r.Route("/profile", func(r chi.Router) {
					r.Use(authCheck.Check)
					r.Get("/", userController.Profile)
				})
				r.Route("/reset_password", func(r chi.Router) {
					r.Post("/", userController.ChangePassword)
				})
				r.Route("/create", func(r chi.Router) {
					r.Post("/", userController.CreateUser)
				})
				r.Route("/get_by_id", func(r chi.Router) {
					r.Get("/", userController.GetByID)
				})
				r.Route("/get_by_email", func(r chi.Router) {
					r.Get("/", userController.GetByEmail)
				})
				r.Route("/verify", func(r chi.Router) {
					r.Post("/", userController.VerifyEmail)
				})
			})
		})
	})

	return r
}

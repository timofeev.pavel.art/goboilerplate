package controller

import (
	"net/http"

	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/infrastructure/component"
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/infrastructure/errors"
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/infrastructure/handlers"
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/infrastructure/responder"
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/modules/user/service"

	"github.com/ptflp/godecoder"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	ChangePassword(w http.ResponseWriter, r *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var req service.ChangePasswordIn
	err := u.Decode(r.Body, &req)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	if len(req.NewPassword) == 0 {
		u.OutputJSON(w, ChangePasswordResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: Data{
				Message: "new password field is empty",
			},
		})
	}

	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	req.UserID = claims.ID

	result := u.service.ChangePassword(r.Context(), req)

	if result.ErrorCode == errors.AuthServiceWrongPasswordErr {
		u.OutputJSON(w, ChangePasswordResponse{
			Success:   false,
			ErrorCode: result.ErrorCode,
			Data: Data{
				Message: "wrong old password",
			},
		})
		return
	}
	if result.ErrorCode != errors.NoError {
		u.OutputJSON(w, ChangePasswordResponse{
			Success:   false,
			ErrorCode: result.ErrorCode,
			Data: Data{
				Message: "wrong old or new password",
			},
		})
		return
	}

	u.OutputJSON(w, ChangePasswordResponse{
		Success:   true,
		ErrorCode: result.ErrorCode,
		Data: Data{
			Message: "password changed successfully",
		},
	})
}

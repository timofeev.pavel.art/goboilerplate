package service

import (
	"context"
	"fmt"

	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/pkg/rest"

	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/infrastructure/component"
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/infrastructure/errors"
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/infrastructure/responder"

	"go.uber.org/zap"

	"github.com/ptflp/godecoder"
)

const (
	BaseURL    = "http://localhost:8081/api/1/user/"
	profileURL = "profile"
	resetPass  = "reset_password"
	create     = "create"
	getByID    = "get_by_id"
	getByEmail = "get_by_email"
	verify     = "verify"
)

type UserService struct {
	Logger *zap.Logger
	client rest.Client
	responder.Responder
	godecoder.Decoder
}

func NewUserService(client rest.Client, components *component.Components) Userer {
	return UserService{
		client:    client,
		Logger:    components.Logger,
		Responder: components.Responder,
		Decoder:   components.Decoder,
	}
}

func (u UserService) Create(ctx context.Context, in UserCreateIn) UserCreateOut {
	var (
		out  CreateRequest
		resp *rest.ApiResponse
		err  error
	)

	resp, err = u.client.SendRequest(ctx, in, create, "POST")
	if err != nil {
		u.Logger.Error("create error: response error", zap.Error(err))
		return UserCreateOut{
			ErrorCode: errors.InternalError,
		}
	}
	if !resp.IsOk {
		u.Logger.Error("get by email error: user server error", zap.Error(fmt.Errorf("%v", resp.ErrorCode)))

		return UserCreateOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}
	defer resp.Response.Body.Close()

	err = u.Decode(resp.Response.Body, &out)
	if err != nil {
		u.Logger.Error("create error: request build error", zap.Error(err))
		return UserCreateOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return UserCreateOut{
		UserID:    out.Data.User.ID,
		ErrorCode: out.ErrorCode,
	}
}

func (u UserService) Update(ctx context.Context, in UserUpdateIn) UserUpdateOut {
	//TODO implement me
	panic("implement me")
}

func (u UserService) VerifyEmail(ctx context.Context, in UserVerifyEmailIn) UserUpdateOut {
	var (
		out  CreateRequest
		resp *rest.ApiResponse
		err  error
	)

	resp, err = u.client.SendRequest(ctx, in, verify, "POST")
	if err != nil {
		u.Logger.Error("verify error: response error", zap.Error(err))
		return UserUpdateOut{
			Success:   false,
			ErrorCode: errors.InternalError,
		}
	}
	if !resp.IsOk {
		u.Logger.Error("get by email error: user server error", zap.Error(fmt.Errorf("%v", resp.ErrorCode)))

		return UserUpdateOut{
			Success:   false,
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}
	defer resp.Response.Body.Close()

	err = u.Decode(resp.Response.Body, &out)

	if err != nil {
		u.Logger.Error("verify error: request build error", zap.Error(err))
		return UserUpdateOut{
			Success:   false,
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return UserUpdateOut{
		Success:   true,
		ErrorCode: out.ErrorCode,
	}
}

func (u UserService) ChangePassword(ctx context.Context, in ChangePasswordIn) ChangePasswordOut {
	var (
		out  CreateRequest
		resp *rest.ApiResponse
		err  error
	)

	resp, err = u.client.SendRequest(ctx, in, resetPass, "POST")
	if err != nil {
		u.Logger.Error("change password error: response error", zap.Error(err))
		return ChangePasswordOut{
			Success:   false,
			ErrorCode: errors.InternalError,
		}
	}
	if !resp.IsOk {
		u.Logger.Error("get by email error: user server error", zap.Error(fmt.Errorf("%v", resp.ErrorCode)))

		return ChangePasswordOut{
			Success:   false,
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}
	defer resp.Response.Body.Close()

	err = u.Decode(resp.Response.Body, &out)
	if err != nil {
		u.Logger.Error("change password error: request build error", zap.Error(err))
		return ChangePasswordOut{
			Success:   false,
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return ChangePasswordOut{
		Success:   true,
		ErrorCode: out.ErrorCode,
	}
}

func (u UserService) GetByEmail(ctx context.Context, in GetByEmailIn) UserOut {
	var (
		out  CreateRequest
		resp *rest.ApiResponse
		err  error
	)

	resp, err = u.client.SendRequest(ctx, in, getByEmail, "GET")
	if err != nil {
		u.Logger.Error("get by email error: response error", zap.Error(err))
		return UserOut{
			ErrorCode: errors.InternalError,
		}
	}
	if !resp.IsOk {
		u.Logger.Error("get by email error: user server error", zap.Error(fmt.Errorf("%v", resp.ErrorCode)))
		return UserOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}
	defer resp.Response.Body.Close()

	err = u.Decode(resp.Response.Body, &out)

	if err != nil {
		u.Logger.Error("get by email error: request build error", zap.Error(err))
		return UserOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return UserOut{
		User:      &out.Data.User,
		ErrorCode: out.ErrorCode,
	}
}

func (u UserService) GetByPhone(ctx context.Context, in GetByPhoneIn) UserOut {
	//TODO implement me
	panic("implement me")
}

func (u UserService) GetByID(ctx context.Context, in GetByIDIn) UserOut {
	var (
		out  CreateRequest
		resp *rest.ApiResponse
		err  error
	)

	resp, err = u.client.SendRequest(ctx, in, getByID, "GET")
	if err != nil {
		u.Logger.Error("get by id error: response error", zap.Error(err))
		return UserOut{
			ErrorCode: errors.InternalError,
		}
	}
	if !resp.IsOk {
		u.Logger.Error("get by email error: user server error", zap.Error(fmt.Errorf("%v", resp.ErrorCode)))

		return UserOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}
	defer resp.Response.Body.Close()

	err = u.Decode(resp.Response.Body, &out)

	if err != nil {
		u.Logger.Error("get by id error: request build error", zap.Error(err))
		return UserOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return UserOut{
		User:      &out.Data.User,
		ErrorCode: out.ErrorCode,
	}
}

func (u UserService) GetByIDs(ctx context.Context, in GetByIDsIn) UsersOut {
	//TODO implement me
	panic("implement me")
}

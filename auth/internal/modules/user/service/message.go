package service

import (
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/models"
)

type Data struct {
	Message string      `json:"message,omitempty"`
	User    models.User `json:"user,omitempty"`
}

type CreateRequest struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"errorCode"`
	Data      Data `json:"data"`
}

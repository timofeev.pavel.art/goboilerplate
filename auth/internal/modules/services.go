package modules

import (
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/infrastructure/component"
	service2 "gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/modules/auth/service"
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/modules/user/service"
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/storages"
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/pkg/rest"
)

type Services struct {
	User service.Userer
	Auth service2.Auther
}

func NewServices(storages *storages.Storages, client rest.Client, components *component.Components) *Services {
	userService := service.NewUserService(client, components)
	return &Services{
		User: userService,
		Auth: service2.NewAuth(userService, storages.Verify, components),
	}
}

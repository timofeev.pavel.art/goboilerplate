package storages

import (
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/db/adapter"
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/modules/auth/storage"
)

type Storages struct {
	Verify storage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		Verify: storage.NewEmailVerify(sqlAdapter),
	}
}

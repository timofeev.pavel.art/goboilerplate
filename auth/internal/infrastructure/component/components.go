package component

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/config"
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/infrastructure/responder"
	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/infrastructure/service"
	cryptography2 "gitlab.com/timofeev.pavel.art/goboilerplate/auth/internal/infrastructure/tools/cryptography"
	"go.uber.org/zap"
)

type Components struct {
	Conf         config.AppConf
	Notify       service.Notifier
	TokenManager cryptography2.TokenManager
	Responder    responder.Responder
	Decoder      godecoder.Decoder
	Logger       *zap.Logger
	Hash         cryptography2.Hasher
}

func NewComponents(conf config.AppConf, notify service.Notifier, tokenManager cryptography2.TokenManager, responder responder.Responder, decoder godecoder.Decoder, hash cryptography2.Hasher, logger *zap.Logger) *Components {
	return &Components{Conf: conf, Notify: notify, TokenManager: tokenManager, Responder: responder, Decoder: decoder, Hash: hash, Logger: logger}
}

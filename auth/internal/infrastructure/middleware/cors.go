package middleware

import (
	"net/http"

	"gitlab.com/timofeev.pavel.art/goboilerplate/auth/config"

	"github.com/go-chi/cors"
)

type Cors struct {
	conf config.Cors
}

func NewCors() *Cors {
	return &Cors{}
}

func (t *Cors) OpenAllCors(next http.Handler) http.Handler {
	return cors.Handler(
		cors.Options{
			AllowedOrigins:   t.conf.AllowedOrigins,
			AllowedMethods:   t.conf.AllowedMethods,
			AllowedHeaders:   t.conf.AllowedHeaders,
			ExposedHeaders:   t.conf.ExposedHeaders,
			AllowCredentials: t.conf.AllowCredentials,
			MaxAge:           t.conf.MaxAge, // Maximum value not ignored by any of major browsers
		},
	)(next)
}

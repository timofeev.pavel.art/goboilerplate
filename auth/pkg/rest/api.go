package rest

import "net/http"

type ApiResponse struct {
	IsOk      bool
	Response  *http.Response
	ErrorCode int
}

package rest

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"go.uber.org/zap"
)

type Client struct {
	BaseURL    string
	HttpClient *http.Client
	Logger     *zap.Logger
}

func NewHttpClient(url string, client *http.Client, logger *zap.Logger) Client {
	return Client{
		BaseURL:    url,
		HttpClient: client,
		Logger:     logger,
	}
}

func (c *Client) SendRequest(ctx context.Context, input interface{}, path string, method string) (*ApiResponse, error) {
	data, err := json.Marshal(input)
	if err != nil {
		c.Logger.Error("request build error", zap.Error(err))
		return nil, fmt.Errorf("marshal error")
	}

	body := bytes.NewReader(data)
	if err != nil {
		c.Logger.Error("request build error", zap.Error(err))
		return nil, fmt.Errorf("body create error")
	}

	req, err := http.NewRequest(method, c.BaseURL+path, body)
	if err != nil {
		c.Logger.Error("request build error", zap.Error(err))
		return nil, fmt.Errorf("request create error")
	}

	reqCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	req = req.WithContext(reqCtx)

	req.Header.Set("Accept", "application/json; charset=utf-8")
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	response, err := c.HttpClient.Do(req)
	if err != nil {
		c.Logger.Error("request build error", zap.Error(err))
		return nil, fmt.Errorf("response error")
	}

	apiResponse := ApiResponse{
		IsOk:     true,
		Response: response,
	}
	if response.StatusCode < http.StatusOK || response.StatusCode >= http.StatusBadRequest {
		apiResponse.IsOk = false
		defer response.Body.Close()

		apiResponse.ErrorCode = response.StatusCode
	}

	return &apiResponse, nil
}
